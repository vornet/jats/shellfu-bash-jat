%global sfcompat __SHELLFU_COMPAT__
%global sfinc %{_datadir}/shellfu/include-%{sfcompat}
%global sfmodn __SHELLFU_MODNAME__
%global shellfu_req shellfu >= __VDEP_SHELLFU_GE__, shellfu < __VDEP_SHELLFU_LT__

Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:            __MKIT_PROJ_VCS_BROWSER__
License:        LGPLv2
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  %shellfu_req
BuildRequires:  make
BuildRequires:  perl
BuildRequires:  shellfu-%{sfcompat}
BuildRequires:  shellfu-bash-jats
BuildRequires:  shellfu-bash-pretty
BuildRequires:  shellfu-bash-sfpi
BuildRequires:  shellfu-sh-isa
BuildRequires:  shellfu-sh-termcolors
BuildRequires:  tar

Requires: %shellfu_req
Requires: perl
Requires: shellfu-%{sfcompat}
Requires: shellfu-bash-arr
Requires: shellfu-bash-bkrh
Requires: shellfu-bash-jats
Requires: shellfu-bash-pretty
Requires: shellfu-bash-sfpi
Requires: shellfu-sh-isa
Requires: shellfu-sh-termcolors
Requires: tar
%description
__SHELLFU_MODDESC__

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr

%check
test -d utils/tfkit || exit 0
make test \
    SHELLFU_PATH=%{buildroot}/%{sfinc}

%files
%doc %{_docdir}/%{name}/README.md
%{sfinc}/%{sfmodn}.sh
%{sfinc}/%{sfmodn}_dump.sh
%{sfinc}/_jat_core.sh
%{sfinc}/_jat_ctxp_foo.sh
%{sfinc}/_jat_ctxp_none.sh
%{sfinc}/_jath.sh
%{sfinc}/_jathp_bkr.sh
%{sfinc}/_jathp_none.sh
%{sfinc}/_jathp_warn.sh
%{sfinc}/jat_ctx.sh


%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
