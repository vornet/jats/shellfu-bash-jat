#!/bin/bash

shellfu import jat

_pretty__debug() {
    local decor="()"
    local lines=()
    local caller_is_main=${caller_is_main:-false}
    local caller=${caller:-UNKNOWN}
    $caller_is_main && decor=
    while IFS= read -r line;
    do lines+=("debug:$caller$decor:$line"); done
    jat__log_info "${lines[@]}"
}


_pretty__die() {
    local lines=()
    while IFS= read -r line;
    do lines+=("$line"); done
    jat__log_error "${lines[@]}"
}


_pretty__mkhelp() {
    local lines=()
    while IFS= read -r line;
    do lines+=("$line"); done
    jat__log_info "${lines[@]}"
}


_pretty__mkusage() {
    local lines=()
    while IFS= read -r line;
    do lines+=("$line"); done
    jat__log_info "${lines[@]}"
}


_pretty__think() {
    local lines=()
    while IFS= read -r line;
    do lines+=("$line"); done
    jat__log_info "${lines[@]}"
}


_pretty__warn() {
    local lines=()
    while IFS= read -r line;
    do lines+=("$line"); done
    jat__log_warning "${lines[@]}"
}
